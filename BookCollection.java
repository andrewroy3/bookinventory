//Main Class File: BookCollection
// This Class File: BookCollection
// Quarter: CSCI 145 Spring 2017 
// Author: Andrew Roy 
// Email: andrewoy3@gmail.com 
// TA's Name: Josh Osborne
// Lab Section: CF 162 Wednesday 10:00


import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

//This program creates a collection of books and processes orders. It also calls queues when there's too many orders and calls editing of those queues
//I have the stocks showing as negative numbers if there's back orders, I thought that would be more informative than having zero

public class BookCollection{

   private Book first; 
   private Book last;
   
   //calls creation of main book collection
   //paramters is file of books
   //no return
   public BookCollection (String bookFile){
      getBooks(bookFile);  
        
   }  
   
   //creates book collection as a linked list
   //paramters is file of books
   //no return
   private void getBooks(String bookFile){
        Book current = first;
        String isbn;
        int stock;
        
        try {

              Scanner input = new Scanner(new File(bookFile));
              
              isbn = input.next();
              stock = input.nextInt();
              current = new Book(isbn, stock);
              addBook(current);

              while (input.hasNextLine()) {
                 
                 isbn = input.next();
                 stock = input.nextInt();
                 current.next = new Book(isbn, stock);  
                 addBook(current);           
                 current = current.next;


              }

        }
        catch(FileNotFoundException e){
         System.out.println("Error");
        }  
   }
   
   //finds the books by cycling through linked list until finding the isbn
   //parameter is isbn
   //returns found book
   private Book findBook(String isbn){
      Book current = first;
      while(current.next != null){
         if ((current.getIsbn()).equals(isbn)){
            return current;
         }
         current = current.next;
      }
      
      current = new Book(isbn, 0);
      addBook(current);
      return current;
   }
   
   //adds a book to collection
   //parameter is book that was created in getBooks
   //no return
   public void addBook(Book book){
      if (first == null){
         first = new Book(book.getIsbn(), book.getStock());
      }
      else{
         Book current = first;
         while (current.next != null){
            current = current.next;
         }
         current.next = new Book(book.getIsbn(), book.getStock());
      }
      
   }
   
   //process Transactions from the given transactions file. Changes stocks and queues and fills orders
   //parameter is file of transactions
   //no return
   public void processTransactions(String transactionsFile){
        BackOrderQueue q1;
        Book current = first;
        int stock;
        int amnt;
        int cust;
        int orgStock;
        int total;
        int reverseTotal;
        String isbn;
        String trns;             
        
        try{
           Scanner data = new Scanner(new File(transactionsFile));
           while (data.hasNextLine()) {
              trns = data.next();
              if(trns.equals("STOCK")){
                  isbn = data.next();
                  stock = data.nextInt();

                  current = findBook(isbn);
                  orgStock = current.getStock();
                  current.changeStock(stock);
                  System.out.println("Stock for "+isbn+" increased from "+orgStock+" to "+current.getStock());
                  
                  current = findBook(isbn);
                  q1 = current.getBackOrderQueue();

                  //use new stock to fill back orders
                  if (q1.isEmpty() == false){
                    q1.edit(q1.front(), stock, isbn);
                  }
                  
              }
              else if(trns.equals("ORDER")){
                  
                  isbn = data.next();
                  amnt = data.nextInt();
                  cust = data.nextInt();                 
                  
                  current = findBook(isbn);
                  q1 = current.getBackOrderQueue();

                  //if stock is negative or zero, must be enqueued
                  if(current.getStock() <= 0){
                     q1.enqueue(cust, amnt);
                     //these if statements are so it prints copy instead of copies before 1
                     if (amnt == 1){
                        System.out.println("Back order of "+amnt+" copy of book "+isbn+" for customer "+cust);
                     }
                     else{
                        System.out.println("Back order of "+amnt+" copies of book "+isbn+" for customer "+cust);
                     }
                     
                     current.changeStock(-amnt);
                  }   
                  else{
                     total = current.getStock() - amnt; 
                     reverseTotal = amnt - current.getStock();//remainder that can't be filled
                     //if amount ordered is more than stock, fill some and queue the rest
                     if (total <= 0){
                        if (current.getStock() == 1){
                           System.out.println("Order filled for customer "+cust+" for "+current.getStock()+" copy of book "+isbn);
                        }
                        else{
                           System.out.println("Order filled for customer "+cust+" for "+current.getStock()+" copies of book "+isbn);
                        }
                        
                        System.out.println("Back order of "+Math.abs(total)+" copies of book "+isbn+" for customer "+cust);
                        q1.enqueue(cust, reverseTotal);
                        current.changeStock(-amnt);
                              
                     }
                     //if amount ordered is less than stock, fill order
                     else{
                        current.changeStock(-amnt);
                        if (amnt == 1){
                           System.out.println("Order filled for customer "+cust+" for "+amnt+" copy of book "+isbn);
                        }
                        else{
                           System.out.println("Order filled for customer "+cust+" for "+amnt+" copies of book "+isbn);
                        }
                     }
                  }
              }
   
           }
        }
        catch(FileNotFoundException e){
         System.out.println("Error");
        }    
   
   }

}
