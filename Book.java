//Main Class File: Book
// This Class File: Book
// Quarter: CSCI 145 Spring 2017 
// Author: Andrew Roy 
// Email: andrewoy3@gmail.com 
// TA's Name: Josh Osborne
// Lab Section: CF 162 Wednesday 10:00


//This program creates and accesses books, along with their stocks, isbn and queues

public class Book{
	
   private String isbn;  
   private int stock; 
   private BackOrderQueue orderQ;
   public Book next;
	
   //constructs a Book
   //parameters are isbn and stock number
   //no return
   Book(String isbn, int startingStock){
      this.isbn = isbn;
      this.stock = startingStock;
      this.orderQ = new BackOrderQueue();
      
   }

   //returns the isbn
   //no parameters
   //returns string of isbn
   public String getIsbn(){
      return this.isbn;
   }
   
   //returns the stock
   //no parameters
   //returns int for stock	
   public int getStock(){
      return this.stock;
   }	

   //returns the queue
   //no parameters
   //returns queue for specific book   
   public BackOrderQueue getBackOrderQueue(){
      return this.orderQ;
   }	

   //changes the stock
   //parameter is amount you want to change it by
   //no return 	
   public void changeStock(int change){
      this.stock = this.stock + change;
   }
      
}