//Main Class File: BackOrderQueue
// This Class File: BackOrderQueue
// Quarter: CSCI 145 Spring 2017 
// Author: Andrew Roy 
// Email: andrewoy3@gmail.com 
// TA's Name: Josh Osborne
// Lab Section: CF 162 Wednesday 10:00

//This program constructs and changes the back order queue for a book

public class BackOrderQueue {
   private BackOrderNode front;
   private BackOrderNode rear;
   
   //node constructors
   private class BackOrderNode{
      private int amount;
      private int customer;
      private BackOrderNode next;
      public BackOrderNode() {
           this(0,0, null);
      }
   
      public BackOrderNode(int customer, int amount) {
          this(customer, amount, null);
      }
   
      public BackOrderNode(int customer, int amount, BackOrderNode next) {
          this.customer = customer;
          this.amount = amount;
          this.next = next;
      }    
   }
   
   //creates BackOrderQueue
   //no parameters
   //no return
   public BackOrderQueue() {
      front = null;
      rear = null;
   } 
   
   //checks if queue is empty or not
   //no parameters
   //returns boolean  
   public boolean isEmpty() {
      return front == null;
   }
   
   //adds an order to the queue
   //parameters are the customer who made the order and the amount they ordered
   //no return
   public void enqueue(int customer, int amount) {
      if (isEmpty()) {
         rear = new BackOrderNode(customer,amount);
         front = rear;
      }

      else {
         rear.next = new BackOrderNode(customer,amount);
         rear = rear.next;
         
      }
   }

   //rmeoves an order from the queue
   //no parameters
   //no return
   public void dequeue() {
      if (isEmpty()){
         throw new RuntimeException("Queue underflow");
      }
      else{
         
         if (front == null){
            rear = null;
         }        
         front = front.next;   
      }
   }
   
   //gets the front order in a queue
   //no parameters
   //returns front node
   public BackOrderNode front() {
      if (isEmpty())
         throw new RuntimeException("Queue underflow");
      return front;
   }
       
   //helps fill orders and notifiy how many there are left to be filled
   //parameters are the order node, the stock of the books that will fill the orders, and the isbn of the book just for the print statement
   //no return
   public void edit(BackOrderNode order, int stock, String isbn){    
      if ((order.amount - stock) > (0) ){
      
         if(stock == 1){
            System.out.println("Order filled for customer "+order.customer+" for "+stock+" copy of book "+isbn);     
         }
         else{
            System.out.println("Order filled for customer "+order.customer+" for "+stock+" copies of book "+isbn); 
         }
         order.amount = order.amount - stock;
         if (order.amount == 1){
            System.out.println("Back order of "+order.amount+" copy of book "+isbn+" for customer "+order.customer);
         }
         else{
            System.out.println("Back order of "+order.amount+" copies of book"+isbn+" for customer "+order.customer);
         }
      }
      
      else{
         int x = stock - order.amount;
         BackOrderNode ne = order.next;
         
         if(order.amount == 1){
            System.out.println("Order filled for customer "+order.customer+" for "+order.amount+" copy of book "+isbn);
         }
         else{
            System.out.println("Order filled for customer "+order.customer+" for "+order.amount+" copies of book "+isbn);
         }   
         this.dequeue();
         if (ne != null){
            edit(ne, x, isbn);
         }   
      }
   }   
      

}